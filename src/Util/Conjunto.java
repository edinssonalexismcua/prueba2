/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Util;

/**
 * Clase contenedora: Cada uno de sus elementos con cajas parametrizadas
 * Conjunto es una estructura da datos estática
 * @author MADARME
 */
public class Conjunto<T> {
    //Estructura de datos estática
    private Caja<T> []cajas;
    private int i=0;
    
    public Conjunto(){}
    
    public Conjunto(int cantidadCajas)
    {
    if(cantidadCajas <=0)
           throw new RuntimeException("No se pueden crear el Conjunto");
        
     this.cajas=new Caja[cantidadCajas];
    }
    
    
    public void adicionarElemento(T nuevo) throws Exception
    {
        if(i >= this.cajas.length)
            throw new Exception("No hay espacio en el Conjunto");
        
        if(this.existeElemento(nuevo))
            throw new Exception("No se puede realizar inserción, elemento repetido");
        
        
        this.cajas[i] = new Caja(nuevo);
        this.i++;
    
    }
    
    public T get(int indice)
    {
        if(indice <0 || indice>=this.getLength())
            throw new RuntimeException("Índice fuera de rango");
        
        return this.cajas[indice].getObjeto();
            
    }
    
    
    public int indexOf(T objBuscar)
    {
    
        for(int j=0;j<i;j++)
        {
            
            //Sacando el estudiante de la caja:
            T x= this.cajas[j].getObjeto();
            
            if(x.equals(objBuscar))
                return j;
        }
        
        return -1;
        
    }
    
    public void set(int indice, T nuevo)
    {
        if(indice <0 || indice>=this.getLength())
            throw new RuntimeException("Índice fuera de rango");
        
        this.cajas[indice].setObjeto(nuevo);
            
    }
    
    
    public boolean existeElemento(T nuevo)
    {
        
        //Sólo estoy comparando por los estudiantes matriculados
        for(int j=0;j<i;j++)
        {
            
            //Sacando el estudiante de la caja:
            T x= this.cajas[j].getObjeto();
            
            if(x.equals(nuevo))
                return true;
        }
        
        return false;
    
    }
    
    
    /**
     *  para el grupo A--> Selección
     *  para el grupo C--> Inserción
     * 
     */
    public void ordenar()//arreglar
    {
        int x;
        Comparable aux;
        for(int i = 1; i < this.cajas.length; i++)
        {
            aux = (Comparable)cajas[i].getObjeto();
            x = i - 1;
            while((x >= 0) && (aux.compareTo(cajas[x].getObjeto()) < 0))
            {
                cajas[x+1].setObjeto(cajas[x].getObjeto());
                x--;
            }
            cajas[x+1] = (Caja)aux;
        }
    }
    
    
    /**
     * Realiza el ordenamiento por burbuja 
     */
    public void ordenarBurbuja()
    {
        Caja aux;
        for(int i = 0; i < this.cajas.length-1; i++)
        {
            for(int x = 0; x < this.cajas.length-i-1; x++)
            {
                Comparable tmp = (Comparable)cajas[x+1].getObjeto();
                if(tmp.compareTo(cajas[x].getObjeto()) < 0)
                {
                    aux = cajas[x+1];
                    cajas[x+1] = cajas[x];
                    cajas [x] = aux;
                }
            }
        }
    }
    
    /**
     * Elimina un elemento del conjunto y compacta
     * @param objBorrado es el objeto que deseo eliminar
     */
    
    public void remover(T objBorrado)
    {
        int m = this.indexOf(objBorrado);
        
        for(int x  = m; x < this.getCapacidad()-1; x++)
        {
            T aux=this.get(x);
            this.set(x, this.get(x+1));
            this.set(x+1, aux);
        }
        cajas[this.i-1] = null;
        this.i--;

    }
    
    /**ntos de nuevo en el conjunto original(this) y 
     * el nuevo queda vacío
     *  El método adiciona todos los elementos de nuevo en el conjunto original(this) y 
     * el nuevo queda vacío. En este proceso no se toma en cuenta los datos repetidos
     * Ejemplo:
     *  conjunto1=<1,2,3,5,6> y conjunto2=<9,1,8>
     * conjunto1.concatenar(conjunto2)
     *  da como resultado: conjunto1=<1,2,3,5,6,9,1,8> y conjunto2=null
     * @param nuevo el objeto conjunto a ser colocado en el conjunto original
     */
    public void concatenar(Conjunto<T> nuevo)
    {
        Caja<T> [] aux = new Caja [this.getCapacidad() + nuevo.getCapacidad()];
        int x = 0;
        int tmp = 0;
        while(x < aux.length)
        {
            if(x < this.getCapacidad())
            {
                aux[x] = this.cajas[x];
                x++;
            }else
            {
                aux[x] = nuevo.cajas[tmp];
                x++;
                tmp++;
            }
        }
        this.i = x;
        nuevo.removeAll();
        this.cajas = aux;
    }
    
    
    /**
     *  El método adiciona todos los elementos de nuevo en el conjunto original(this) y 
     * el nuevo queda vacío. En este proceso SI toma en cuenta los datos repetidos
     * Ejemplo:
     *  conjunto1=<1,2,3,5,6> y conjunto2=<9,1,8>
     * conjunto1.concatenar(conjunto2)
     *  da como resultado: conjunto1=<1,2,3,5,6,9,8> y conjunto2=null
     * @param nuevo el objeto conjunto a ser colocado en el conjunto original
     */
    public void concatenarRestrictivo(Conjunto<T> nuevo)
    {
        this.concatenar(nuevo);
        for(int h = 0; h < this.getCapacidad()-1; h++)
        {
            Caja<T> pivote = new Caja(cajas[h].getObjeto());
            for(int x = h+1; x < this.getCapacidad(); x++)
            {
                if(pivote.getObjeto().equals(cajas[x].getObjeto()))
                {
                    compactar(x);
                    cajas[this.getCapacidad()-1] = null;
                    i--;
                }
            }
        }
    }
    
    public void compactar(int indice){
        for(int x = indice; x < this.getCapacidad()-1; x++)
        {
            Caja<T> aux = cajas[x];
            cajas[x] = cajas[x+1];
            cajas[x+1] = aux;
        }
        
    }
    
    public void removeAll()
    {
        this.cajas=null;
        this.i=0;
    }
    
    
    @Override
    public String toString() {
        String msg="******** CONJUNTO*********\n";
        
        for(Caja c:this.cajas)
            if(c != null)
                msg+=c.getObjeto().toString()+" ";
        
        msg+="\n";
        
        return msg;
    }
    
    
    
    /**
     * Obtiene la cantidad de elementos almacenados
     * @return  retorna un entero con la cantidad de elementos
     */
    public int getCapacidad()
    {
        return this.i;
    }
    
    /**
     *  Obtiene el tamaño máximo de cajas dentro del Conjunto
     * @return int con la cantidad de cajas
     */
    public int getLength()
    {
        return this.cajas.length;
    }
    
    /**
     * Obtiene el mayor elemento del Conjunto, recordar que el conjunto no posee elementos repetidos.
     * @return el elemnto mayor de la colección
     */
    public T getMayorElemento()
    {
    if(this.cajas==null)
        throw new RuntimeException("No se puede encontrar elemento mayor, el conjunto está vacío");
    
    T mayor=this.cajas[0].getObjeto();
    for(int i=1;i<this.getCapacidad();i++)
    {
        //Utilizo la interfaz comparable y después su método compareTo
        Comparable comparador=(Comparable)mayor;
        T dato_A_comparar=this.cajas[i].getObjeto();
        // Resta entra mayor-datoAComparar 
        int rta_compareTo=comparador.compareTo(dato_A_comparar);
        if(rta_compareTo<0)
            mayor=dato_A_comparar;
    }
    return mayor;
    }
}